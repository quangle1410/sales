package com.quanglv.config;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "file")
@Data
public class FilesConfig {

    private String templateDirectory;
    private String rootDir;
    private String publicDirectory;
    private String privateDirectory;
}
