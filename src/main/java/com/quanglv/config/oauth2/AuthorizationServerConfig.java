package com.quanglv.config.oauth2;


import com.quanglv.constant.AuthConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.AccessTokenConverter;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import javax.sql.DataSource;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;

@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private BCryptPasswordEncoder encoder;

//    @Bean
//    public JwtAccessTokenConverter accessTokenConverter() {
//        JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
//        converter.setSigningKey("as466gf");
//        return converter;
//    }

//    @Bean
//    public TokenStore tokenStore() throws NoSuchAlgorithmException {
//        return new JwtTokenStore(accessTokenConverter());
//    }

    @Override
    public void configure(ClientDetailsServiceConfigurer configurer) throws Exception {

        configurer
                .inMemory()
                .withClient(AuthConstants.CLIENT_ID)
                .secret(encoder.encode(AuthConstants.CLIENT_SECRET))
                .authorizedGrantTypes(AuthConstants.GRANT_TYPE_PASSWORD,
                        AuthConstants.AUTHORIZATION_CODE,
                        AuthConstants.REFRESH_TOKEN,
                        AuthConstants.IMPLICIT )
                .scopes(AuthConstants.SCOPE_READ,
                        AuthConstants.SCOPE_WRITE,
                        AuthConstants.TRUST)
                .accessTokenValiditySeconds(AuthConstants.ACCESS_TOKEN_VALIDITY_SECONDS)
                .refreshTokenValiditySeconds(AuthConstants.REFRESH_TOKEN_VALIDITY_SECONDS);
    }

//    @Override
//    public void configure(AuthorizationServerEndpointsConfigurer endpoints) {
//        endpoints.tokenStore(tokenStore())
//                .authenticationManager(authenticationManager)
//                .accessTokenConverter(accessTokenConverter());
//    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints.authenticationManager(authenticationManager);
        endpoints.tokenStore(tokenStore());
        endpoints.accessTokenConverter(accessTokenConverter());
    }

    @Bean
    public KeyPair keyPair() throws NoSuchAlgorithmException {
        KeyPair keyPair = KeyPairGenerator.getInstance(AuthConstants.RSA).generateKeyPair();
        return keyPair;
    }

    @Bean
    public AccessTokenConverter accessTokenConverter() throws NoSuchAlgorithmException {
        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter();
        accessTokenConverter.setKeyPair(keyPair());
        return accessTokenConverter;
    }

    @Bean
    TokenStore tokenStore() {
        return new InMemoryTokenStore();
    }

}
