package com.quanglv.constant;

public interface AuthConstants {
    String CLIENT_ID = "client_id";
    String CLIENT_SECRET = "client_secret";
    //        String CLIENT_SECRET = "$2a$04$e/c1/RfsWuThaWFCrcCuJeoyvwCV0URN/6Pn9ZFlrtIWaU/vj/BfG";
    String GRANT_TYPE_PASSWORD = "password";
    String AUTHORIZATION_CODE = "authorization_code";
    String REFRESH_TOKEN = "refresh_token";
    String IMPLICIT = "implicit";
    String SCOPE_READ = "read";
    String SCOPE_WRITE = "write";
    String TRUST = "openid";
    int ACCESS_TOKEN_VALIDITY_SECONDS = 5 * 60;
    int REFRESH_TOKEN_VALIDITY_SECONDS = 5 * 60;

    String RSA = "RSA";

    String RESOURCE_ID = "resource_id";

    String ACTIVE_STATUS = "ACTIVE";
    Long INACTIVE_STATUS = 0L;

    String AUTH_CONTEXT = "/api/**";
}
