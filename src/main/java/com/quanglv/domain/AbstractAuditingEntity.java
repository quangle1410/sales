package com.quanglv.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;
import java.util.Date;

@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
@Data
@EqualsAndHashCode
public abstract class AbstractAuditingEntity implements Serializable {

    private static final long serialVersionUID = 6957172879983027097L;

    @CreatedDate
    @Column(name = "maker_time")
    private Instant createdTime;

    // @CreatedBy
    @Column(name = "maker")
    private Long maker;

    @LastModifiedDate
    @Column(name = "updated_time")
    private Instant updatedTime;

    // @LastModifiedBy
    @Column(name = "updater")
    private Long updater;
}
