package com.quanglv.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "file_storages")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileStorages extends AbstractAuditingEntity {
    @Id
//    @SequenceGenerator(name = "file_storage_seq", allocationSize = 1)
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "file_storage_seq")
    @Column(name = "id", nullable = false)
    @NotNull
    private String id;

    @Column(name = "path")
    private String filePath;

    @Column(name = "name")
    private String fileName;

    @Column(name = "extension")
    private String fileExtension;

    @Column(name = "size")
    private Long fileSize;

    @Column(name = "type")
    private String fileType;

    @Column(name = "storage_days")
    private Integer storageDays;

    @Column(name = "status")
    private String status;
}
