package com.quanglv.service;

public interface SendMail {
    void sendEmailAsync(String to, String subject, String text);
}
