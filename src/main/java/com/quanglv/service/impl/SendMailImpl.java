package com.quanglv.service.impl;

import com.quanglv.config.email.EmailConfiguration;
import com.quanglv.service.SendMail;
import com.quanglv.service.ThymeleafService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class SendMailImpl implements SendMail {

    private static final String CONTENT_TYPE_TEXT_HTML = "text/html;charset=\"utf-8\"";

    @Qualifier("getJavaMailSender")
    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private ThymeleafService thymeleafService;

    @Autowired
    private EmailConfiguration emailConfiguration;

    public void sendEmail(String to, String subject, String text) {
        try {
            SimpleMailMessage message = new SimpleMailMessage();
            message.setTo(to);
            message.setSubject(subject);
            message.setText(text);

            javaMailSender.send(message);
        } catch (MailException exception) {
            exception.printStackTrace();
        }
    }

    public void sendEmailHtml(String to, String subject, String text) {

        Properties properties = System.getProperties();
        properties.put("mail.smtp.host", emailConfiguration.getMailServerHost());
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.port", emailConfiguration.getMailServerPort());
        Session session = Session.getInstance(properties,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailConfiguration.mailServerUsername, emailConfiguration.mailServerPassword);
                    }
                });
        Message message = new MimeMessage(session);
        try {
            message.setRecipients(Message.RecipientType.TO, new InternetAddress[]{new InternetAddress(to)});

            message.setFrom(new InternetAddress(
                    emailConfiguration.getMailServerUsername()));
            message.setSubject(subject);
            message.setContent(thymeleafService.getContent(), CONTENT_TYPE_TEXT_HTML);
            Transport.send(message);
        } catch (MessagingException e) {
            e.getMessage();
        }
    }

    @Override
    public void sendEmailAsync(String to, String subject, String text) {
//        CompletableFuture.runAsync(() ->{
            sendEmailHtml(to, subject, text);
//        });
    }
}
